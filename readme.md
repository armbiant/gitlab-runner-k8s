# Gitlab runner on Kubernetes

## Config

Most of the config in `values.yaml` already has sensible values, but some changes are required for new setups:

- `runnerRegistrationToken`, copy it from your Gitlab project or group > Settings > CI / CD > Runners
- `runnerToken`: keep it empty when registering a new runner, but copy it from Gitlab for an already registered runner.


Note, don't push updates to `values.yaml`! A useful trick is to do:

```
git update-index --skip-worktree values.yaml
```

## Installation

Install `helm` and `kubectl` (e.g. `snap install kubectl helm`).

```
helm template gitlab-runner gitlab-runner -f values.yaml > gitlab-runner.yaml
kubectl apply -f gitlab-runner.yaml
kubectl get all
```

Notice: on helm prior to 3.0 you might need to use `helm template -n gitlab-runner gitlab-runner ...` instead.

There should now be a deployment of gitlab-runner-gitlab-runner-something.

## Bumping the gitlab-runner version

Checkout a new version of the helm chart in the `gitlab-runner/` in the submodule and modify `.gitmodules`'s branch accordingly and maybe fix the `values.yaml`. And run

```
helm template gitlab-runner gitlab-runner -f values.yaml > gitlab-runner.yaml
kubectl apply -f gitlab-runner.yaml
```

again.
